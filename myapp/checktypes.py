from re import (
    compile,
    match,
)

from django.apps import AppConfig
from django.conf import settings
from django.core.checks.messages import (
    DEBUG,
    ERROR,
    INFO,
    WARNING,
    CheckMessage,
)
from mypy import api

"""
The check framework is used for multiple different kinds of checks.
As such, errors and warnings can originate from models or other django objects.
The `CheckMessage` requires an object as the source of the message
and so we create a temporary object that simply displays the file and line number from mypy (i.e. "location")
"""


class MyPyErrorLocation:
    """
    Simple error that only display location
    """
    def __init__(self, location):
        self.location = location

    def __str__(self):
        return self.location


def mypy(app_configs: list[AppConfig], **kwargs) -> list[CheckMessage]:
    print("Performing mypy checks...")
    results = api.run([str(settings.BASE_DIR)])
    error_messages = results[0]
    if not error_messages:
        return []
    # Example: myproject/checks.py:17: error: Need type annotation for 'errors'
    pattern = compile(r'^(.+\d+): (\w+): (.+)')
    errors = []
    for message in error_messages.rstrip().split('\n'):
        parsed = match(pattern, message)
        if not parsed:
            continue
        location = parsed.group(1)
        mypy_level = parsed.group(2)
        message = parsed.group(3)
        level = DEBUG
        if mypy_level == 'note':
            level = INFO
        elif mypy_level == 'warning':
            level = WARNING
        elif mypy_level == 'error':
            level = ERROR
        else:
            print(f"Unrecognized mypy level: {mypy_level}")
        errors.append(CheckMessage(level, message, obj=MyPyErrorLocation(location)))
    return errors
