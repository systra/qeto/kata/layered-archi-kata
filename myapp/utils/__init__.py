from django.conf import settings

try:
    version = (settings.BASE_DIR / 'version').read_text().strip()
except Exception:
    version = 'unknown'
