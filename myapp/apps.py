from django.apps import AppConfig as SuperAppConfig


class AppConfig(SuperAppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'myapp'
