from django.http import JsonResponse

from .utils import version


def get_version(request):
    return JsonResponse({
        'version': version,
    })
