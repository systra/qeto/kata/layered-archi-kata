from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .views import get_version

urlpatterns = [
    path('version', csrf_exempt(get_version), name='version'),
]
