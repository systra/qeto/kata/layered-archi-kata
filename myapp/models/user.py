from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    def get_full_name(self):
        return f"{self.first_name} {self.last_name}" if self.first_name else self.last_name

    @property
    def full_name(self):
        return self.get_full_name()

    def __str__(self):
        if self.is_superuser:
            return self.get_full_name() or self.get_username()
        else:
            return self.get_full_name() or self.email or self.get_username()
