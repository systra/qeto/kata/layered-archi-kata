from graphene import (
    ObjectType,
    Schema,
)

from .api.category import (
    CategoryNode,
    CreateCategory,
    DeleteCategory,
    UpdateCategory,
)
from .api.comment import (
    CommentNode,
    CreateComment,
    DeleteComment,
    UpdateComment,
)
from .api.post import (
    CreatePost,
    DeletePost,
    PostNode,
    UpdatePost,
)
from .api.user import UserNode
from .graphene import (
    ListField,
    NodeField,
)


class Query(ObjectType):
    user = NodeField(UserNode)
    all_users = ListField(UserNode)
    category = NodeField(CategoryNode)
    all_categories = ListField(CategoryNode)
    post = NodeField(PostNode)
    all_posts = ListField(PostNode)
    comment = NodeField(CommentNode)
    all_comments = ListField(CommentNode)


class Mutation(ObjectType):
    create_category = CreateCategory.Field()
    update_category = UpdateCategory.Field()
    delete_category = DeleteCategory.Field()
    create_post = CreatePost.Field()
    update_post = UpdatePost.Field()
    delete_post = DeletePost.Field()
    create_comment = CreateComment.Field()
    update_comment = UpdateComment.Field()
    delete_comment = DeleteComment.Field()


schema = Schema(query=Query, mutation=Mutation)
