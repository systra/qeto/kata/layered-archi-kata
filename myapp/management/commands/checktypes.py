from sys import exit

from django.core.management.base import BaseCommand
from app.checktypes import mypy


class Command(BaseCommand):
    help = 'Check type hints with mypy'

    def handle(self, *args, **options):
        msgs = mypy([])
        if msgs:
            for msg in msgs:
                colorize = self.style.ERROR if msg.is_serious() else self.style.WARNING
                self.stdout.write(colorize(msg))
            exit(1)
        else:
            exit(0)
