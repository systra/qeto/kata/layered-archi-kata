from dataclasses import dataclass


@dataclass
class SetRoomPriceCommand:
    room_number: int
    hotel_name: str
    new_room_price: float
