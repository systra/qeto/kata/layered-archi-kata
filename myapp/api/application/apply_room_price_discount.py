from typing import Protocol
from .apply_room_price_discount_command import ApplyRoomPriceDiscountCommand
from ..domain.room_collection import RoomCollection
from ..domain.price import Price


class ApplyRoomPriceDiscount(Protocol):
    def execute(self, command: ApplyRoomPriceDiscountCommand):
        ...


class ApplyRoomPriceDiscountImpl(ApplyRoomPriceDiscount):
    def __init__(self, room_collection: RoomCollection):
        self.room_collection = room_collection

    def execute(self, command: ApplyRoomPriceDiscountCommand) -> None:
        room = self.room_collection.get(command.hotel_name, command.room_number)
        if room:
            room.price = Price(room.price.value * (1 - command.discount))
            self.room_collection.save(room)
