from dataclasses import dataclass


@dataclass
class ApplyRoomPriceDiscountCommand:
    room_number: int
    hotel_name: str
    discount: float
