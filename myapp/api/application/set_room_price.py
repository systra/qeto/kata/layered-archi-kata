from typing import Protocol
from .set_room_price_command import SetRoomPriceCommand
from ..domain.room_collection import RoomCollection
from ..domain.price import Price


class SetRoomPrice(Protocol):
    def execute(self, command: SetRoomPriceCommand):
        ...


class SetRoomPriceImpl(SetRoomPrice):
    def __init__(self, room_collection: RoomCollection):
        self.room_collection = room_collection

    def execute(self, command: SetRoomPriceCommand) -> None:
        room = self.room_collection.get(command.hotel_name, command.room_number)
        if room:
            room.price = Price(command.new_room_price)
            self.room_collection.save(room)
