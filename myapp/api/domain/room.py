from dataclasses import dataclass
from .price import Price


@dataclass
class Room:
    number: int
    price: Price
