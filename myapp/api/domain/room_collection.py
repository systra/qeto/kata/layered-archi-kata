from typing import (
    Protocol,
    Optional,
)
from .room import Room


class RoomCollection(Protocol):
    def get(self, hotel_name: str, room_number: int) -> Optional[Room]:
        ...

    def save(self, room: Room) -> None:
        ...
