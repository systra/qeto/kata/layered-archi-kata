from .negative_room_price_exception import NegativeRoomPriceException


class Price:
    def __init__(self, value: float):
        if value < 0:
            raise NegativeRoomPriceException(value)
        self._value = value

    def __eq__(self, other):
        if isinstance(other, Price):
            return self.value == other.value
        return False

    @property
    def value(self):
        return self._value
