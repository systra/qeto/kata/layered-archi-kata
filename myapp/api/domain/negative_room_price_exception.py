class NegativeRoomPriceException(Exception):

    def __init__(self, price: float):
        super().__init__(f"The room price {price} is incorrect: room price cannot be negative")
