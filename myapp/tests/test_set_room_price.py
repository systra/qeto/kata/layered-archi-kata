from dataclasses import replace
from typing import Optional
from unittest import TestCase

from myapp.api.application.apply_room_price_discount import (
    ApplyRoomPriceDiscount,
    ApplyRoomPriceDiscountImpl,
)
from myapp.api.application.apply_room_price_discount_command import ApplyRoomPriceDiscountCommand
from myapp.api.application.set_room_price import (
    SetRoomPrice,
    SetRoomPriceImpl,
)
from myapp.api.application.set_room_price_command import SetRoomPriceCommand
from myapp.api.domain.negative_room_price_exception import NegativeRoomPriceException
from myapp.api.domain.price import Price
from myapp.api.domain.room import Room
from myapp.api.domain.room_collection import RoomCollection


class RoomCollectionFake(RoomCollection):
    def __init__(self, room: Room):
        self.room = room
        self._saved = False

    def get(self, hotel_name: str, room_number: int) -> Optional[Room]:
        return self.room

    def save(self, room: Room) -> None:
        self.room = replace(room)
        self._saved = True


class TestSetRoomPrice(TestCase):
    def test_should_set_room_price(self):
        # GIVEN
        room_number = 114
        new_room_price_value = 42
        hotel_name = 'Hotel California'
        room = Room(room_number, Price(10.0))
        room_collection = RoomCollectionFake(room)
        use_case: SetRoomPrice = SetRoomPriceImpl(room_collection)
        # WHEN
        use_case.execute(SetRoomPriceCommand(
            hotel_name=hotel_name,
            room_number=room_number,
            new_room_price=new_room_price_value,
        ))
        # THEN
        room_after_save = room_collection.get(hotel_name, room_number)
        self.assertIsNotNone(room_after_save)
        self.assertEqual(room_after_save.price, Price(new_room_price_value))

    def test_should_not_allow_negative_room_price(self):
        # GIVEN
        room_number = 114
        new_room_price_value = -14.2
        hotel_name = 'Hotel California'
        room = Room(room_number, Price(10.0))
        room_collection = RoomCollectionFake(room)
        use_case: SetRoomPrice = SetRoomPriceImpl(room_collection)
        # WHEN / THEN
        with self.assertRaises(NegativeRoomPriceException) as error_cm:
            use_case.execute(SetRoomPriceCommand(
                hotel_name=hotel_name,
                room_number=room_number,
                new_room_price=new_room_price_value,
            ))
        # THEN
        self.assertEqual(str(error_cm.exception), "The room price -14.2 is incorrect: room price cannot be negative")
        room_after_save = room_collection.get(hotel_name, room_number)
        self.assertIsNotNone(room_after_save)
        self.assertEqual(room_after_save.price, Price(10.0))
        self.assertFalse(room_collection._saved)


class TestSetRoomDiscount(TestCase):
    def test_should_apply_discount(self):
        # GIVEN
        room_number = 176
        discount = 0.2
        hotel_name = 'Hotel California'
        room = Room(room_number, Price(35.0))
        room_collection = RoomCollectionFake(room)
        use_case: ApplyRoomPriceDiscount = ApplyRoomPriceDiscountImpl(room_collection)
        # WHEN
        use_case.execute(ApplyRoomPriceDiscountCommand(
            hotel_name=hotel_name,
            room_number=room_number,
            discount=discount,
        ))
        # THEN
        room_after_save = room_collection.get(hotel_name, room_number)
        self.assertIsNotNone(room_after_save)
        self.assertEqual(room_after_save.price, Price(35 * (1 - discount)))
