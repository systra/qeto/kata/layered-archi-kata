from operator import (
    le,
    lt,
)

from django.db.models import BooleanField
from django_filters import (
    BaseCSVFilter,
    BooleanFilter,
    Filter,
)
from django_filters import FilterSet as OrigFilterSet
from django_filters import MultipleChoiceFilter
from graphene import Connection as OrigConnection
from graphene import (
    Int,
    Node,
)
from graphene.types.scalars import (
    MAX_INT,
    MIN_INT,
)
from graphene_django.filter import DjangoFilterConnectionField as OrigDjangoFilterConnectionField
from graphene_django.filter import GlobalIDFilter
from graphene_django.filter.filterset import (
    custom_filterset_factory,
    setup_filterset,
)
from graphene_django.filter.utils import get_filtering_args_from_filterset
from graphene_django.forms.converter import convert_form_field
from graphene_django.forms.forms import (
    GlobalIDFormField,
    GlobalIDMultipleChoiceField,
)
from graphql.language.ast import IntValue
from graphql_relay import from_global_id

# https://docs.djangoproject.com/en/3.1/ref/models/querysets/#field-lookups
STR_FILTER = [
    'isnull',
    'isempty',
    'exact',
    'iexact',
    'ne',
    'contains',
    'icontains',
    'startswith',
    'istartswith',
    'endswith',
    'iendswith',
    'in',
    'regex',
    'iregex',
]
ID_FILTER = [
    'isnull',
    'exact',
    'in',
]
DEF_FILTER = [
    'isnull',
    'exact',
]
INT_FILTER = [
    'isnull',
    'exact',
    'in',
    'gt',
    'gte',
    'lt',
    'lte',
    'range',
]
DATETIME_FILTER = [
    'isnull',
    'exact',
    'in',
    'gt',
    'gte',
    'lt',
    'lte',
    'range',
    'date',
    'year',
    'iso_year',
    'month',
    'day',
    'week',
    'week_day',
    'iso_week_day',
    'quarter',
    'time',
    'hour',
    'minute',
    'second',
]


class RangeInt(Int):
    min_int = MIN_INT
    strict_min = False
    max_int = MAX_INT
    strict_max = False

    @classmethod
    def bounds(cls, min=None, strict_min=None, max=None, strict_max=None):
        min_int = cls.min_int if min is None else min
        strict_min = cls.strict_min if strict_min is None else strict_min
        max_int = cls.max_int if max is None else max
        strict_max = cls.strict_max if strict_max is None else strict_max
        subclass = type(
            f'RangeInt_{min_int}{"S" if strict_min else ""}_{max_int}{"S" if strict_max else ""}',
            (cls,),
            {
                'min_int': min_int,
                'strict_min': strict_min,
                'max_int': max_int,
                'strict_max': strict_max,
            },
        )
        return subclass

    @classmethod
    def get_min_op(cls):
        return lt if cls.strict_min else le

    @classmethod
    def get_max_op(cls):
        return lt if cls.strict_max else le

    @classmethod
    def is_between_bounds(cls, num):
        return cls.get_min_op()(cls.min_int, num) and cls.get_max_op()(num, cls.max_int)

    @classmethod
    def coerce_range_int(cls, value):
        try:
            num = int(value)
        except ValueError:
            try:
                num = int(float(value))
            except ValueError:
                return None
        if cls.is_between_bounds(num):
            return num

    serialize = coerce_range_int
    parse_value = coerce_range_int

    @classmethod
    def parse_literal(cls, ast):
        if isinstance(ast, IntValue):
            num = int(ast.value)
            if cls.is_between_bounds(num):
                return num


PositiveInt = RangeInt.bounds(min=0)
PositiveStrictInt = PositiveInt.bounds(strict_min=True)


# patch filterset_class in DjangoFilterConnectionField,
# which used to badly patch/convert BaseCSVFilter to Filter but with loosing the original type
# BEGIN_PATCH
class PatchedGlobalIDMultipleChoiceFilter(MultipleChoiceFilter):
    """
    Keep an iterable value in each element of the filter (SQL 'in') value.
    """
    field_class = GlobalIDMultipleChoiceField

    def filter(self, qs, value: list[str]):
        gids: tuple[str, ...] = tuple(from_global_id(v)[1] for v in value)
        wrapped_gids: tuple[tuple[str, ...], ...] = (gids,) if gids else ()
        return super().filter(qs, wrapped_gids)


def replace_csv_filters(filterset_class):
    """
    Replace the "in" and "range" filters (that are not explicitly declared) to not be BaseCSVFilter (BaseInFilter, BaseRangeFilter) objects anymore
    but regular Filter objects that simply use the input value as filter argument on the queryset.

    This is because those BaseCSVFilter are expecting a string as input with comma separated value but with GraphQl we
    can actually have a list as input and have a proper type verification of each value in the list.

    See issue https://github.com/graphql-python/graphene-django/issues/1068.

    AutoField are badly handled (no formField for this kind of field), so keep original subtype.
    """
    for name, filter_field in filterset_class.base_filters.items():
        filter_type = filter_field.lookup_expr
        if filter_type in ["in", "range"]:
            assert isinstance(filter_field, BaseCSVFilter)
            # patched here
            if filter_field.field_class and issubclass(filter_field.field_class, GlobalIDFormField):
                filterset_class.base_filters[name] = PatchedGlobalIDMultipleChoiceFilter(
                    field_name=filter_field.field_name,
                    lookup_expr=filter_field.lookup_expr,
                    label=filter_field.label,
                    method=filter_field.method,
                    exclude=filter_field.exclude,
                    **filter_field.extra
                )
            else:
                filterset_class.base_filters[name] = Filter(
                    field_name=filter_field.field_name,
                    lookup_expr=filter_field.lookup_expr,
                    label=filter_field.label,
                    method=filter_field.method,
                    exclude=filter_field.exclude,
                    **filter_field.extra
                )


def get_correct_filterset_class(filterset_class, **meta):
    """
    Get the class to be used as the FilterSet.
    Patch BaseCSVFilter to ordinary Filter but keep original subtype.
    """
    # vanilly from upstream
    if filterset_class:
        # If were given a FilterSet class, then set it up.
        graphene_filterset_class = setup_filterset(filterset_class)
    else:
        # Otherwise create one.
        graphene_filterset_class = custom_filterset_factory(**meta)
    replace_csv_filters(graphene_filterset_class)
    return graphene_filterset_class


def patched_filterset_class(self):
    # vanilly from upstream
    if not self._filterset_class:
        fields = self._fields or self.node_type._meta.filter_fields
        meta = dict(model=self.model, fields=fields)
        if self._extra_filter_meta:
            meta.update(self._extra_filter_meta)
        filterset_class = self._provided_filterset_class or (
            self.node_type._meta.filterset_class
        )
        self._filterset_class = get_correct_filterset_class(filterset_class, **meta)
    return self._filterset_class


setattr(OrigDjangoFilterConnectionField, 'filterset_class', property(patched_filterset_class))
# END_PATCH


class Connection(OrigConnection):
    class Meta:
        abstract = True

    count = Int()
    total_count = Int()

    def resolve_count(root, info, **kwargs):
        return len(root.edges)

    def resolve_total_count(root, info, **kwargs):
        return root.length


class FilterSet(OrigFilterSet):
    @classmethod
    def filter_for_lookup(cls, field, lookup_type):
        DEFAULTS = dict(cls.FILTER_DEFAULTS)
        if hasattr(cls, '_meta'):
            DEFAULTS.update(cls._meta.filter_overrides)
        filter_class, params = super().filter_for_lookup(field, lookup_type)
        # fix for isempty
        if lookup_type == 'isempty':
            data = DEFAULTS[BooleanField]
            filter_class = data.get('filter_class')
            params = data.get('extra', lambda field: {})(field)
        return filter_class, params


def create_custom_method_filters(filter_name, base_filter_class, lookups, func):
    custom_method_name = f'filter_{filter_name}'

    def custom_method(self, queryset, name, value):
        return func(queryset, name, value)
    boolean_lookups = ('isnull', 'isempty')
    in_lookups = ('in',)
    range_lookups = ('range',)
    field_class_map = {
        GlobalIDFilter: GlobalIDMultipleChoiceField,
    }

    def get_list_filter_attrs(filter_class):
        return {
            'field_class': field_class_map.get(filter_class, filter_class.field_class),
        }
    return {
        f'{filter_name}__{lookup}' if lookup != 'exact' else filter_name: base_filter_class(method=custom_method_name)
        for lookup in lookups
        if lookup not in boolean_lookups + in_lookups + range_lookups
    } | {
        f'{filter_name}__{lookup}': BooleanFilter(method=custom_method_name)
        for lookup in lookups
        if lookup in boolean_lookups
    } | {
        f'{filter_name}__{lookup}': type('InFilter', (BaseCSVFilter,), get_list_filter_attrs(base_filter_class))(
            lookup_expr=lookup,
            method=custom_method_name,
        )
        for lookup in lookups
        if lookup in in_lookups
    } | {
        f'{filter_name}__{lookup}': type('RangeFilter', (BaseCSVFilter,), get_list_filter_attrs(base_filter_class))(
            lookup_expr=lookup,
            method=custom_method_name,
        )
        for lookup in lookups
        if lookup in range_lookups
    } | {
        custom_method_name: custom_method,
    }


class DjangoFilterConnectionField(OrigDjangoFilterConnectionField):
    @property
    def filtering_args(self):
        args = get_filtering_args_from_filterset(self.filterset_class, self.node_type)
        # fix for isempty
        for name, filter_field in self.filterset_class.base_filters.items():
            if all((
                name not in self.filterset_class.declared_filters,
                filter_field.lookup_expr in ('isnull', 'isempty'),
            )):
                field_type = convert_form_field(filter_field.field).Argument()
                field_type.description = filter_field.label
                args[name] = field_type
        return args


ListField = DjangoFilterConnectionField
NodeField = Node.Field


__all__ = [
    'STR_FILTER',
    'ID_FILTER',
    'DEF_FILTER',
    'INT_FILTER',
    'DATETIME_FILTER',
    'PositiveInt',
    'PositiveStrictInt',
    'RangeInt',
    'Connection',
    'FilterSet',
    'DjangoFilterConnectionField',
    'ListField',
    'NodeField',
]
