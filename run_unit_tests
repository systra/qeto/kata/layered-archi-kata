#!/bin/bash
set -e
cd "$(dirname "$(readlink -f "$0")")"
if [ -n "$QUICK" ]; then
    DO_FORK=
    NO_FLAKE8=1
    NO_COVERAGE=1
    NO_XUNIT=1
    REUSE_DB=1
fi
# shellcheck disable=SC2089
PYTEST_ADDOPTS="--cache-clear --metadata 'Back Git version' \"$(git name-rev --name-only HEAD)\""

if [ -z "$NO_DB" ]; then
    echo 'select 1' | PGPASSWORD=myapp psql -h 127.0.0.1 -p "${DJANGO_DB_PORT:-5432}" -U myapp -w myapp >/dev/null 2>&1 && NO_DB=1
    if [ -z "$NO_DB" ]; then
        if [ -z "$NO_DOCKER" ] && which docker >/dev/null; then
            echo "Running postgres in docker"
            pg_cid=$(./run_docker_postgres "${DJANGO_DB_PORT:-5432}" daemon)
            trap 'docker rm -f $pg_cid >/dev/null 2>&1' EXIT INT TERM
        elif which postgres >/dev/null && which initdb >/dev/null; then
            echo "Running postgres with local postgres/initdb"
            pg_dir=/tmp/postgres$$
            pg_pid=$(./run_local_postgres "${DJANGO_DB_PORT:-5432}" $pg_dir daemon|tail -n1)
            trap '[ -n "$pg_pid" ] && kill $pg_pid; sleep 1; rm -rf "$pg_dir"' EXIT INT TERM
        else
            echo "No way to run a Postgres database" >&2
            exit 1
        fi
    fi
fi

if [ -n "$DO_FORK" ]; then
    REUSE_DB=1
    PYTEST_ADDOPTS="$PYTEST_ADDOPTS --forked -nauto -v"
fi
if [ -n "$REUSE_DB" ]; then
    PYTEST_ADDOPTS="$PYTEST_ADDOPTS --reuse-db"
fi
if [ -z "$NO_FLAKE8" ]; then
    PYTEST_ADDOPTS="$PYTEST_ADDOPTS --flake8"
fi
if [ -z "$NO_COVERAGE" ]; then
    [ -d reports/coverage ] && rm -rf reports/coverage
    PYTEST_ADDOPTS="$PYTEST_ADDOPTS --cov=myapp --cov-report=term --cov-report=xml"
fi
if [ -z "$NO_XUNIT" ]; then
    PYTEST_ADDOPTS="$PYTEST_ADDOPTS --junitxml=reports/report.xunit --html=reports/report.html --self-contained-html"
fi

# shellcheck disable=SC2090
export PYTEST_ADDOPTS
export DJANGO_DEBUG=False
export DJANGO_DB_HOST=localhost
[ -n "$DJANGO_DB_PORT" ] || export DJANGO_DB_PORT=5432
./manage.py test "$@"
ret=$?
if [ -e reports/.coverage ]; then
    coverage html
fi
exit $ret
