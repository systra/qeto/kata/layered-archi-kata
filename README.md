# Visator

[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

## Setup

```sh
# create a virtual env with python version 3.9 or more
pipenv --python 3.9 shell
# install dependencies (and development dependencies, `-d`, if you want)
pipenv sync -d
# setup database schema and configuration
./manage.py migrate
```

## Run server

```sh
# serve with hot reload at localhost:8000
./manage.py runserver_plus
```

You can also change the port by adding the port number as last argument of `runserver_plus`.

Then go to [http://localhost:8000/].

## Environment variables

The following environment variables are available with their default:

| Name                         | Default value                           | Comment                    |
| ---                          | ---                                     | ---                        |
| `DJANGO_DEBUG`               | `True`                                  | `0`/`1`/`True`/`False`     |
| `DJANGO_LOG_LEVEL`           | `INFO` if `DJANGO_DEBUG` else `WARNING` |                            |
| `DJANGO_RUNSERVER_LOG_LEVEL` | `DEBUG` if `DJANGO_DEBUG` else `INFO`   | only used with `runserver` |
| `DJANGO_HTTPS`               | `False`                                 | `0`/`1`/`True`/`False`     |
| `DJANGO_SECRET_KEY`          | *default value for dev only*            | change this                |


## Run unit tests

You can execute unit tests by using the `run_unit_test`. This script will create a temporary Postgres database in order to carry out all unit tests and then execute all test cases.

You can run a group or a single test by indicating path to the test
```sh
./run_unit_tests -v 2 [path to the test file]::[Test class]::[name of the test]
```

_Example:_
```sh
./run_unit_tests -v 2 tests/api/test_codification.py::TestCodification::test__get_codification_with_input_id
```

### Quick run and options

By default, if you run tests with `./run_unit_tests`, some operations like coverage and linter will be executed.

If you wish to run unit tests quickly without using all these options, you can use the `QUICK` option as follows:

```sh
QUICK=1 ./run_unit_tests
```

You can also put value to 1 to the following environment variables in order to disable some operations.

```sh
DO_FORK
REUSE_DB
NO_FLAKE8
NO_COVERAGE
NO_XUNIT
```

If you want to let a database running between tests (to speed it up), run either `./run_docker_postgres 5432 daemon` or `./run_local_postgres 5432 daemon`.

You can ommit the `daemon` if you want to keep the terminal open on the db process. You can then stop it with `Ctrl-C`.

### Details

You can indicate the detail level on the error or warning messages by using the `-v` option.

```sh
QUICK=1 ./run_unit_tests -v 2
```

### Pytest options

You can pass other `pytest` options by terminating `django` options with `--`.
You can for instance get the `pytest` help by doing:

```sh
./run_unit_tests -- --help
```

One commond option is `-s` to get all print output, even if the tests succeeds.
